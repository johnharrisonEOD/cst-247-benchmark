﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Benchmark.Models
{
    public class VerseModel
{
        [Required(ErrorMessage ="Testament is required")]
        public string Testament { get; set; }

        [Required(ErrorMessage = "Book is required")]
        public string Book { get; set; }

        [Required(ErrorMessage = "Chapter is required")]
        public int Chapter { get; set; }

        [Required(ErrorMessage = "Verse Number is required")]
        public int VerseNumber { get; set; }

        [Required(ErrorMessage = "Verse Text is required")]
        public string VerseText { get; set; }


}
}
