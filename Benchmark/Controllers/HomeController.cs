﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Benchmark.Models;
using static DataLibrary.BusinessLogic.ProcessVerse;

namespace Benchmark.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Search()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Search(VerseModel model)
        {
            if(ModelState.IsValid)
            {
                var data = SearchBible(model.Testament, model.Book, model.Chapter, model.VerseNumber, model.VerseText);
                if (data.Count >= 1)
                {
                    List<VerseModel> verses = new List<VerseModel>();

                    foreach (var row in data)
                    {
                        verses.Add(new VerseModel
                        {
                            Testament = row.TESTAMENT,
                            Book = row.BOOK,
                            Chapter = row.CHAPTER,
                            VerseNumber = row.VERSE_NUM,
                            VerseText = row.VERSE_TEXT
                        });
                    }

                    return View("SearchResults",verses);
                }
                else
                {
                    return View("SearchFail");
                }
            }
            return View();
        }

        public ActionResult ViewBibleVerses()
        {
            ViewBag.Message = "Bible Verses";
            var data = LoadBible();
            List<VerseModel> verses = new List<VerseModel>();

            foreach(var row in data)
            {
                verses.Add(new VerseModel
                {
                    Testament = row.TESTAMENT,
                    Book = row.BOOK,
                    Chapter = row.CHAPTER,
                    VerseNumber = row.VERSE_NUM,
                    VerseText = row.VERSE_TEXT
                });
            }

            return View(verses);
        }

        public ActionResult AddBibleVerse()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddBibleVerse(VerseModel model)
        {
            if(ModelState.IsValid)
            {
                int record = NewBibleVerse(model.Testament, model.Book, model.Chapter, model.VerseNumber, model.VerseText);
                return RedirectToAction("Index");
            }
            return View();
        }
    }
}